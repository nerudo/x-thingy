

# Answer Log:
1. ![Architecture diagram](./Documentation/x-mart-architecture.jpg "Achitecture Diagram")
2. Domain Driven Microservices Architecture.
3. ./
4. Database definied in ``docker-compose.services.yml`` creation and restore through EFCore.
``dotnet ef``
5. Solution : ``namespace Products.API.DAL`` file ``ProductsRepository.cs``

## Known issues

- Startup errors
- Service provisioning scripts
- Missing (RabitMQ integration)

# Improvements

- Impliment domain events to handle adding of products and impliment inventory control through pub-sub
- Impliment integration events for inter-service communication

# Architecture description

- Domain Driven Microservices (Type)
- AWS / Azure agnostic (PAAS Provider)
- Kubernettes (Ochestration)
