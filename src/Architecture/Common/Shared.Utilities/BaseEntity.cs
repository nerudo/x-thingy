﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.Utilities
{
    public class BaseEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        public string EntityStatus { get; set; }
        public DateTime? Created { get; set; }
    }
}
