﻿using System;
using System.Net;

namespace Shared.Utilities
{

    public class ServiceResponse : ServiceResponse<object>
    { }

    public class ServiceResponse<T> where T : class
    {
        public HttpStatusCode Status { get; set; } = HttpStatusCode.OK;
        public string Message { get; set; }
        public T Data { get; set; } = null;
        public object Error { get; set; } = null;
    }
}
