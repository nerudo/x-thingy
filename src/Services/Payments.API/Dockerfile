#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:3.1 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/sdk:3.1 AS build
WORKDIR /src
COPY ["src/Services/Payments.API/Payments.API.csproj", "src/Services/Payments.API/"]
COPY ["src/Architecture/Common/Shared.Utilities/Shared.Utilities.csproj", "src/Architecture/Common/Shared.Utilities/"]
RUN dotnet restore "src/Services/Payments.API/Payments.API.csproj"
COPY . .
WORKDIR "/src/src/Services/Payments.API"
RUN dotnet build "Payments.API.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Payments.API.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Payments.API.dll"]
