﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Products.API.DAL;
using Products.API.DAL.Resources;

namespace Products.API.Services.Entities
{
    public class ProductsService : IProductsService
    {
        private readonly ProductsRepository productsRepository;
        public ProductsService(ProductsRepository context)
        {
            productsRepository = context;
        }
        public Task<List<Product>> ListAll()
        {
            return productsRepository.ListAsync();
        }
        public Product GetById(Guid id)
        {
            return productsRepository.FindOne(id);
        }
        public async Task<List<Product>> SearchByName(string keyword)
        {
            return await productsRepository.SearchByName(keyword);
        }
        public Product Add(ProductsDTO productsDTO)
        {
            var c = new Product
            {
                Name = productsDTO.Name,
                Cost = productsDTO.Cost,
                Price = productsDTO.Price
            }; //TODO: Change to use Automapper

            return productsRepository.AddAndSaveChanges(c);
        }
    }
}
