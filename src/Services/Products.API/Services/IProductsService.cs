﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Products.API.DAL;

namespace Products.API.Services
{
    public interface IProductsService
    {
        Product GetById(Guid id);
        Task<List<Product>> SearchByName(string keyword);
        Task<List<Product>> ListAll();
    }
}
