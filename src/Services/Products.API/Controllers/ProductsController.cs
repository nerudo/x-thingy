﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Products.API.DAL;
using Products.API.DAL.Resources;
using Products.API.Services.Entities;
using Shared.Utilities;

namespace Products.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly ProductsService _products;

        public ProductsController(ILogger<ProductsController> logger, ProductsService products)
        {
            _logger = logger;
            _products = products;
        }

        [HttpGet]
        [Route("ListAll")]
        public async Task<ActionResult<ServiceResponse>> ListAll()
        {
            var data = await _products.ListAll();
            return new ServiceResponse { Data = data };
        }

        [HttpGet]
        [Route("[action]")]
        public ServiceResponse GetByID(Guid productId)
        {
            var data = _products.GetById(productId);
            return new ServiceResponse { Data = data };
        }

        [HttpGet]
        [Route("[action]")]
        public ServiceResponse Search(string keyword)
        {
            var data = _products.SearchByName(keyword);
            return new ServiceResponse { Data = data };
        }

        [HttpPost]
        [Route("Add")]
        public ServiceResponse AddProductAsync([FromBody] ProductsDTO productDTO)
        {
            var product = _products.Add(productDTO);
            _logger.LogInformation("Sending New Product Added Integration event");
            //TODO: Send out Product Added Integration Event
            //var scEvent = new ProductAddedIntegrationEvent(product);
            //await _integrationEventService.SaveEventAndMissionStationContextChangesAsync(scEvent);
            //await _integrationEventService.PublishThroughEventBusAsync(scEvent);
            return new ServiceResponse { Data = product };
        }
    }
}
