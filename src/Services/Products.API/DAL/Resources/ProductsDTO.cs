﻿using System;
namespace Products.API.DAL.Resources
{
    public class ProductsDTO
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public Uri Image { get; set; }
    }
}
