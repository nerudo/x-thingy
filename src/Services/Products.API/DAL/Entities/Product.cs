﻿using System;
using Shared.Utilities;

namespace Products.API.DAL
{
    public class Product : BaseEntity
    {
        public Product()
        {
        }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public Uri Image { get; set; }
    }
}
