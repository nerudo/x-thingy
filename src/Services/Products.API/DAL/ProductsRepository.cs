﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Products.API.DAL.Resources;

namespace Products.API.DAL
{
    public class ProductsRepository
    {
        private readonly RepositoryContext _context;
        public ProductsRepository(RepositoryContext context)
        {
            _context = context;
        }
        public Task<List<Product>> ListAsync()
        {
            return _context.Products.ToListAsync();
        }

        public Product FindOne(Guid id)
        {
            return _context.Products.Where(c => c.Id == id).FirstOrDefault();
        }

        public Task<List<Product>> SearchByName(string keyword)
        {
            return _context.Products.Where(s => s.Name.Contains(keyword.ToUpper())).ToListAsync();
        }
        public Product AddAndSaveChanges(Product product)
        {
            _context.Products.Add(product);
            _context.SaveChanges();
            return product;
        }
        public async Task<bool> PatchUpdate(Guid id, ProductsDTO product)
        {
            _context.Entry(await _context.Products.FirstOrDefaultAsync(x => x.Id == id)).CurrentValues.SetValues(product);
            return (await _context.SaveChangesAsync()) > 0;
        }
    }
}
